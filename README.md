# Activation Function Impact on CNN Edge Inference
### Martin Power
### 9939245


### This repo contains the following experiments:
* Experiment 01 - Sparsity Profile for ResNet-50
* Experiment 02 - Comparison of Activation Functions for Simple CNN
* Experiment 03 - Comparison of Activation Functions for ResNet-RS
 